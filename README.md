# Open PixelFont

PixelFont contains pixel source file of fonts licensed under "SIL OFL V1.1" or "GPLv2+FE"

This fonts are optimized to be displayed on a monochrome lcd with low resolution.
Generatred with Seggers FontCvtST from STM and afterwards hand-optimized.

The font heigth are 
* AATUniCJK: 17px
* AATSans:   17px


# Reference

The original TTF files used in this project an be found at the following locations

* GNU Unifont: unifont-12.0.01.ttf     [web page](http://unifoundry.com/unifont/index.html)
* PT Sans:     PT_Sans-Web-Regular.ttf [Google Fonts](https://fonts.google.com/specimen/PT+Sans)


# Licence Terms

The source file of this fonts shall be published ander the same license as thery original TTF files are:
* GNU Unifont --> AATUniCJK: [`GPLv2+FE`](https://gitlab.com/aat_hoh/pixelfont/blob/master/AATUniCJK/LICENSE.txt)
* PT Sans     --> AATSans  : [`SIL OFL V1.1`](https://gitlab.com/aat_hoh/pixelfont/blob/master/AATSans/LICENSE.txt)

